e3-cfhvms
======
ESS Site-specific EPICS module : cfhvms (*Convential Facilities High-Voltage Monitoring System*)

## Introduction
cfhvms is a purely soft IOC that is provided to "monitor" the status of the Conventional Facilities High-Voltage system. The IOC performs no write operations.

In reality, the monitoring of the HV system is perfomed by a SCADA ABB server operated and maintained by CF. This IOC implements the translation of signals from the CF monitoring solution into the EPICs domain for display in the main control room.

The CF server broadcasts the signals using the OPC UA protocol, via a gateway between the CF VLAN and EPICs on the Technical Network. An NTP server connected to the timing system enables timestamping of the data as close to the source as possible [1].

Technical requirements for the CFHVMS system are defined in the system requirement specification [2].

|![cfhvms_interfaces](docs/cfhvms_interfaces.png)|
| :---: |
|**Figure 1** Overview of the CFHVMS interfaces [1] |

For more information on the CFHVMS interfaces see the interface control document [3].

## Supported architectures
Due to the dependency on e3-opcua, which relies on pre-compiled shared libraries, this module currently only supports the Linux-x86_64 architecture. Specifically, CentOS 7 only.

If you have access to the Unified Automation OPC UA C++ SDK, it is possible to compile the required shared libraries for other architectures [5].

## Dependencies
* e3-opcua (v0.8.0 or later) - https://gitlab.esss.lu.se/e3/common/e3-opcua

## Build and install the module
```
make build
make install
```

## Usage
To start the IOC for the production server 
```
cd cfhvsms/cmds
iocsh.bash st.cmd
```

To start the IOC for the test server
```
cd test
iocsh.bash st_test.cmd
```

## Database
The IOC loads the following record sets:
* One primary substation
* 20 high-voltage substations
* 60 low-voltage substations

The record sets are populated via macro expansion. The following variables are used:

**OPC environment variables**

| Variable  | Description |
| -         | - |
| SESSION   |Name for session (connection to server) [user-defined] |
| SUBSCRIPT |Name for subscription (subset of signals to monitor) [user-defined] |

**OPC production server variables**

| Variable     | Description |
| -            | - |
| OPCSERVER    | IP address of ABB SCADA OPC UA server |
| OPCPORT      | port number and path to OPC UA server instance |
| OPCNAMESPACE | OPC UA namespace containing to monitor signals |

**Low-voltage substation variables**

| Variable | Description |
| -        | - |
| SUBST    | Substation identifier |

**High-voltage substation variables**

| Variable   | Description |
| -          | - |
| SUBSTATION | Substation identifier |
| SLOT       | Slot identifier |

For further information regarding the database structure and expansion see [cfhvms/cfhvmsApp/Db](cfhvms/cfhvmsApp/Db)
## Timestamping

All PVs source their timestamp via device support, i.e. from the timestamp data provided by the ABB SCADA server. This mechanism is expected to provide a time accuracy of a few milliseconds [1].

## Testing
In order to simulate the data from the SCADA ABB server, a simulation server is used.

A simulation iocsh file (cfhvms_sim.iocsh) is provided to export a subset (one HV and one LV substation) of signals to the EPICS environment.

For more information on the configuration of the simulation server, see [simulation instructions](test/server/README.md).

The unit tests for the CFHVSMS module are defined using pytest and pyepics, see [test](test). The test script implements the unit tests defined in the verfication and validation plan [4].

## References
[1] [\[ESS-0145298\]](https://chess.esss.lu.se/enovia/link/ESS-0145298/21308.51166.57344.20452/valid) CF HV Monitoring System - System Design Description.

[2] [\[ESS-0145297\]](https://chess.esss.lu.se/enovia/link/ESS-0145297/21308.51166.44800.28081/valid) CF HV Monitoring System - Requirement Specification

[3] [\[ESS-0145299\]](https://chess.esss.lu.se/enovia/link/ESS-0145299/21308.51166.60928.56848/valid) CF HV Monitoring System - Interface Control Document

[4] [\[ESS-0145304\]](https://chess.esss.lu.se/enovia/link/ESS-0145304/21308.51166.51456.27357/valid) CF HV Monitoring System - Verification and Validation Plan

[5] [\[e3-opcua\]](https://gitlab.esss.lu.se/e3/common/e3-opcua) E3 OPC UA EPICS module repository
